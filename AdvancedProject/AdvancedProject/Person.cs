﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AdvancedProject
{
    public class Person
    {
        public string ManagerMasterKey
            {
            get;private set;
            }
        private int _idNumber;
        public string Name
        {
            get; set;
        }
        public int IdNumber
        {
            get
            {
                return _idNumber;
            }
            set
            {
                if (!idExists(value)) _idNumber = value;
            }
        }
        public string Email
        {
            get; set;
        }
        public string PhoneNumber
        {
            get; set;
        }
        public string RoomNumber
        {
            get; set;
        }
        public TypeOfPerson Role
        {
            get; set;
        }

        private List<int> ids = new List<int>();

        /*public Person()
        {
            ids.Add(GenerateId());
        }*/
        public Person(string name, string phoneNumber, string email, string roomNumber, TypeOfPerson role)
        {
            ManagerMasterKey = "Xlhd55#f";
            Name = name;
            PhoneNumber = phoneNumber;
            Email = email;
            RoomNumber = roomNumber;
            Role = role;
            ids = new List<int>();
            _idNumber = GenerateId();
            ids.Add(_idNumber);
        }

        private bool idExists(int idNumber)
        {
            bool exists = false;
            foreach (var item in ids)
            {
                if (idNumber == item)
                {
                    exists = true;
                }
            }
            if (!exists)
            {
                return false;
            }
            else
            {
                return true;
            }
        }
        private int GenerateId()
        {
            Random rand = new Random();
            int id;
            do
            {
                id = rand.Next(100000, 999999);

            }
            while (ids.Contains(id));
            return id;
        }
    }
}