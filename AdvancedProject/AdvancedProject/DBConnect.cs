﻿using MySql.Data.MySqlClient;
using Dapper;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data.SqlClient;
using System.Data;

namespace AdvancedProject
{
    class DBConnect
    {
        private MySqlConnection connection;
        private string server;
        private string database;
        private string uid;
        private string password;




        //Constructor
        public DBConnect()
        {
            Initialize();
        }

        //Initialize values
        private void Initialize()
        {
            server = "studmysql01.fhict.local";
            database = "dbi427502";
            uid = "dbi427502";
            password = "Wum3H-OzX0";
            string connectionString;
            connectionString = "SERVER=" + server + ";" + "DATABASE=" +
            database + ";" + "UID=" + uid + ";" + "PASSWORD=" + password + ";";

            connection = new MySqlConnection(connectionString);
        }

        private bool OpenConnection()
        {
            try
            {
                connection.Open();
                return true;
            }
            catch (MySqlException ex)
            {
                //When handling errors, you can your application's response based 
                //on the error number.
                //The two most common error numbers when connecting are as follows:
                //0: Cannot connect to server.
                //1045: Invalid user name and/or password.
                switch (ex.Number)
                {
                    case 0:

                        //MessageBox.Show("Cannot connect to server.  Contact administrator");
                        break;

                    case 1045:
                        //MessageBox.Show("Invalid username/password, please try again");
                        break;
                }
                return false;
            }
        }

        //Close connection
        private bool CloseConnection()
        {
            try
            {
                connection.Close();
                return true;
            }
            catch (MySqlException ex)
            {
                //MessageBox.Show(ex.Message);
                return false;
            }
        }
        //Insert statement
        public void Insert(string query)
        {
            //string query = "INSERT INTO tableinfo (name, age) VALUES('John Smith', '33')";

            //open connection
            if (this.OpenConnection() == true)
            {
                //create command and assign the query and connection from the constructor
                MySqlCommand cmd = new MySqlCommand(query, connection);

                //Execute command
                cmd.ExecuteNonQuery();

                //close connection
                this.CloseConnection();
            }
        }

        //Update statement
        public void Update(string query)
        {
            //string query = "UPDATE tableinfo SET name='Joe', age='22' WHERE name='John Smith'";

            //Open connection
            if (this.OpenConnection() == true)
            {
                //create mysql command
                MySqlCommand cmd = new MySqlCommand();
                //Assign the query using CommandText
                cmd.CommandText = query;
                //Assign the connection using Connection
                cmd.Connection = connection;

                //Execute query
                cmd.ExecuteNonQuery();

                //close connection
                this.CloseConnection();
            }
        }

        //Delete statement
        public void Delete(string query)
        {
            //string query = "DELETE FROM tableinfo WHERE name='John Smith'";

            if (this.OpenConnection() == true)
            {
                MySqlCommand cmd = new MySqlCommand(query, connection);
                cmd.ExecuteNonQuery();
                this.CloseConnection();
            }
        }
        public string NameByID(int id)
        {
            if (this.OpenConnection() == true)
            {
                MySqlDataReader myReader;
                MySqlCommand myCommand = new MySqlCommand($"SELECT Name FROM `students` WHERE identification = {id}", connection);
                myReader = myCommand.ExecuteReader();
                while (myReader.Read())
                {
                    string name = myReader["Name"].ToString();
                    this.CloseConnection();
                    return name;
                }
                myReader.Close();
                myCommand = new MySqlCommand($"SELECT Name FROM `managers` WHERE verification = {id}", connection);
                myReader = myCommand.ExecuteReader();
                while (myReader.Read())
                {
                    string name = myReader["Name"].ToString();
                    this.CloseConnection();
                    return name;
                }
                //close connection
                this.CloseConnection();

            }
            return "";
        }
        public string BuildingByID(int id)
        {
            if (this.OpenConnection() == true)
            {
                MySqlDataReader myReader;
                MySqlCommand myCommand = new MySqlCommand($"SELECT Building FROM `students` WHERE identification = {id}", connection);
                myReader = myCommand.ExecuteReader();
                while (myReader.Read())
                {
                    string buildingName = myReader["Building"].ToString();
                    this.CloseConnection();
                    return buildingName;
                }
                myReader.Close();
                //close connection
                this.CloseConnection();
            }
            return "";
        }
        public bool RoomAvailable(string room, string building)
        {
            if (this.OpenConnection() == true)
            {
                MySqlDataReader myReader;
                MySqlCommand myCommand = new MySqlCommand($"SELECT Room FROM `students` WHERE Building = '{building}'", connection);
                myReader = myCommand.ExecuteReader();
                while (myReader.Read())
                {
                    if (myReader["Room"].ToString() == room)
                    {
                        this.CloseConnection();
                        return false;
                    }
                }
                myReader.Close();
                //close connection
                this.CloseConnection();
            }
            return true;
        }
        public string[] EventByIndex(int index)
        {
            string[] eventDetails = new string[5];
            if (this.OpenConnection() == true)
            {
                MySqlDataReader myReader;
                MySqlCommand myCommand = new MySqlCommand($"SELECT Person, Title, Description, Date, Type FROM `events` WHERE id = '{index}'", connection);
                myReader = myCommand.ExecuteReader();

                while (myReader.Read())
                {
                    eventDetails[0] = myReader["Person"].ToString();
                    eventDetails[1] = myReader["Title"].ToString();
                    eventDetails[2] = myReader["Description"].ToString();
                    eventDetails[3] = myReader["Date"].ToString();
                    eventDetails[4] = myReader["Type"].ToString();
                }
                myReader.Close();

                //close connection
                this.CloseConnection();
            }
            return eventDetails;
        }
        public string[] MessagesByBuilding(string building)
        {
            List<string> messages = new List<string>();
            if (this.OpenConnection() == true)
            {
                MySqlDataReader myReader;
                MySqlCommand myCommand = new MySqlCommand($"SELECT Person, Message FROM `chat` WHERE Building  = '{building}'", connection);
                myReader = myCommand.ExecuteReader();
                while (myReader.Read())
                {
                    messages.Add($"{myReader["Person"]}:\t{myReader["Message"]}");
                }
                this.CloseConnection();
                return messages.ToArray();
            }
            return null;
        }
        public string[] MandatoryEvents()
        {
            List<String> mandEvents = new List<string>();
            if (this.OpenConnection() == true)
            {
                MySqlDataReader myReader;
                MySqlCommand myCommand = new MySqlCommand($"SELECT id, Person, Title, Description, Date FROM `events` WHERE Type = 'Mandatory'", connection);
                myReader = myCommand.ExecuteReader();
                while (myReader.Read())
                {
                    string result = $"{myReader["id"]} Name: " + myReader["Person"].ToString() + ",\t";
                    result += "Title: " + myReader["Title"].ToString() + ",\t";
                    result += "Description: " + myReader["Description"].ToString() + ",\t";
                    result += "Date: " + myReader["Date"].ToString() + ",";
                    mandEvents.Add(result);
                }
                myReader.Close();

                //close connection
                this.CloseConnection();
            }
            return mandEvents.ToArray();
        }
        public string[] Complaints()
        {
            List<String> allComplaints = new List<string>();
            if (this.OpenConnection() == true)
            {
                MySqlDataReader myReader;
                MySqlCommand myCommand = new MySqlCommand($"SELECT * FROM `complaints`", connection);
                myReader = myCommand.ExecuteReader();
                while (myReader.Read())
                {
                    string result = $"{myReader["id"]},\t";
                    result += "Title: " + myReader["Title"].ToString() + ",\t";
                    result += "Description: " + myReader["Description"].ToString() + ",\t";
                    result += "Date: " + myReader["Date"].ToString() + ",";
                    allComplaints.Add(result);
                }
                myReader.Close();

                //close connection
                this.CloseConnection();
            }
            return allComplaints.ToArray();
        }

        public string[] AllStudents()
        {
            List<String> studentsList = new List<string>();
            if (this.OpenConnection() == true)
            {
                MySqlDataReader myReader;
                MySqlCommand myCommand = new MySqlCommand($"SELECT Name FROM `students`", connection);
                myReader = myCommand.ExecuteReader();
                while (myReader.Read())
                {                   
                    studentsList.Add(myReader["Name"].ToString());
                }
                myReader.Close();

                //close connection
                this.CloseConnection();
            }
            return studentsList.ToArray();
        }


        public string[] EventsByName(string name)
        {
            List<String> userEvents = new List<string>();
            if (this.OpenConnection() == true)
            {
                MySqlDataReader myReader;
                MySqlCommand myCommand = new MySqlCommand($"SELECT id, Person, Title, Description, Date FROM `events` WHERE Person = '{name}'", connection);
                myReader = myCommand.ExecuteReader();

                while (myReader.Read())
                {
                    string result = $"{myReader["id"]} Name: " + myReader["Person"].ToString() + ",\t";
                    result += "Title: " + myReader["Title"].ToString() + ",\t";
                    result += "Description: " + myReader["Description"].ToString() + ",\t";
                    result += "Date: " + myReader["Date"].ToString() + ",";
                    userEvents.Add(result);
                }
                myReader.Close();

                //close connection
                this.CloseConnection();
            }
            return userEvents.ToArray();
        }
        public string SelectPCheck(int id)
        {
            //Open connection
            if (this.OpenConnection() == true)
            {
                MySqlDataReader myReader = null;
                MySqlCommand myCommand = new MySqlCommand("SELECT identification FROM `students`", connection);
                myReader = myCommand.ExecuteReader();
                while (myReader.Read())
                {
                    if (myReader["identification"].ToString() == id.ToString())
                    {
                        this.CloseConnection();
                        return "S";
                    }
                }
                myReader.Close();
                myCommand = new MySqlCommand("SELECT verification FROM `managers`", connection);
                myReader = myCommand.ExecuteReader();
                while (myReader.Read())
                {
                    if (myReader["verification"].ToString() == id.ToString())
                    {
                        this.CloseConnection();
                        return "M";
                    }
                }
                //close connection
                this.CloseConnection();
                return "NA";
            }
            return "ERR";
        }

    }
}


