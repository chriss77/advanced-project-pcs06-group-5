﻿namespace AdvancedProject
{
    partial class ChatForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(ChatForm));
            this.chatLb = new System.Windows.Forms.ListBox();
            this.messageRTb = new System.Windows.Forms.RichTextBox();
            this.sendBtn = new System.Windows.Forms.Button();
            this.closeBtn = new System.Windows.Forms.Button();
            this.chatTimer = new System.Windows.Forms.Timer(this.components);
            this.SuspendLayout();
            // 
            // chatLb
            // 
            this.chatLb.FormattingEnabled = true;
            this.chatLb.HorizontalScrollbar = true;
            this.chatLb.ItemHeight = 20;
            this.chatLb.Location = new System.Drawing.Point(12, 12);
            this.chatLb.Name = "chatLb";
            this.chatLb.Size = new System.Drawing.Size(776, 364);
            this.chatLb.TabIndex = 0;
            // 
            // messageRTb
            // 
            this.messageRTb.Location = new System.Drawing.Point(12, 382);
            this.messageRTb.Name = "messageRTb";
            this.messageRTb.Size = new System.Drawing.Size(636, 62);
            this.messageRTb.TabIndex = 2;
            this.messageRTb.Text = "";
            // 
            // sendBtn
            // 
            this.sendBtn.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("sendBtn.BackgroundImage")));
            this.sendBtn.Font = new System.Drawing.Font("Century Gothic", 11F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.sendBtn.ForeColor = System.Drawing.Color.Linen;
            this.sendBtn.Location = new System.Drawing.Point(654, 378);
            this.sendBtn.Name = "sendBtn";
            this.sendBtn.Size = new System.Drawing.Size(134, 31);
            this.sendBtn.TabIndex = 32;
            this.sendBtn.Text = "Send";
            this.sendBtn.UseVisualStyleBackColor = true;
            this.sendBtn.Click += new System.EventHandler(this.sendBtn_Click);
            // 
            // closeBtn
            // 
            this.closeBtn.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("closeBtn.BackgroundImage")));
            this.closeBtn.Font = new System.Drawing.Font("Century Gothic", 11F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.closeBtn.ForeColor = System.Drawing.Color.Linen;
            this.closeBtn.Location = new System.Drawing.Point(654, 415);
            this.closeBtn.Name = "closeBtn";
            this.closeBtn.Size = new System.Drawing.Size(134, 31);
            this.closeBtn.TabIndex = 33;
            this.closeBtn.Text = "Close";
            this.closeBtn.UseVisualStyleBackColor = true;
            this.closeBtn.Click += new System.EventHandler(this.closeBtn_Click);
            // 
            // chatTimer
            // 
            this.chatTimer.Interval = 1000;
            this.chatTimer.Tick += new System.EventHandler(this.chatTimer_Tick);
            // 
            // ChatForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(9F, 20F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(800, 450);
            this.Controls.Add(this.closeBtn);
            this.Controls.Add(this.sendBtn);
            this.Controls.Add(this.messageRTb);
            this.Controls.Add(this.chatLb);
            this.Name = "ChatForm";
            this.Text = "ChatForm";
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.ListBox chatLb;
        private System.Windows.Forms.RichTextBox messageRTb;
        private System.Windows.Forms.Button sendBtn;
        private System.Windows.Forms.Button closeBtn;
        private System.Windows.Forms.Timer chatTimer;
    }
}