﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace AdvancedProject
{
    public partial class Student_Change : Form
    {
        DBConnect dBConnect;
        string[] eventDetails;
        private int id;
        public Student_Change(int id)
        {
            InitializeComponent();
            this.id = id;
            dBConnect = new DBConnect();
            eventDetails = dBConnect.EventByIndex(id);
            tbName_Student_Change.Text = eventDetails[1];
            tbDescription_Student_Change.Text = eventDetails[2];
           
        }

        private void btnEdit_Student_Change_Click(object sender, EventArgs e)
        {
            if (eventDetails[4] == "Personal")
            {
                dBConnect.Update($"UPDATE `events` SET `id`={id},`Person`='{eventDetails[0]}',`Title`='{tbName_Student_Change.Text}',`Description`='{tbDescription_Student_Change.Text}',`Date`='{dateTimePickerStudentChange.Value.ToString("yyyy-MM-dd")}',`Type`='Personal' WHERE id = '{id}'");
            }
            else 
            {
                MessageBox.Show("Only Personal events can be edited");
            }
           
        }

        private void btnDelete_Student_Change_Click(object sender, EventArgs e)
        {
            if (eventDetails[4] == "Personal")
            {
                dBConnect.Delete($"DELETE FROM `events` WHERE id = '{id}'");
            }
            else
            {
                MessageBox.Show("Only Personal events can be deleted");
            }
        }


       
    }
}
