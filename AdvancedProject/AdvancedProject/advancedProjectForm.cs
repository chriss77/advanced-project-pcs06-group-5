﻿using Microsoft.VisualBasic;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace AdvancedProject
{
    public partial class LoginRegistration : Form
    {
        DBConnect dBConnect;
        public LoginRegistration()
        {
            InitializeComponent();
            dBConnect = new DBConnect();
            StudenPnl.Visible = false;
            registrationPnl.Visible = false;
            ManagerPnl.Visible = false;
            timerStudent.Start();            
        }
        #region Initial view
        private void LoginRegistration_Load(object sender, EventArgs e)
        {
            //resize the Lauch Panel and everything that it contains
            launchPnl.SetBounds(0, 0, 1154, 680);
            //picture box
            pb_LogIn_Form.SetBounds(0, 1, 1138, 640);
            //the ID TextBox
            idTb.SetBounds(406, 242, 332, 42);
            //Button LogIn
            logInBtn.SetBounds(435, 300, 266, 42);
            //Button Register
            goToRegisterBtn.SetBounds(435, 350, 266, 42);            
            Height = launchPnl.Height;
            Width = launchPnl.Width;
            MaximizeBox = false;
            CenterToScreen();
        }

        private void goToRegisterBtn_Click(object sender, EventArgs e)
        {
            launchPnl.Visible = false;
            registrationPnl.Visible = true;

            //panel
            registrationPnl.SetBounds(0, 1, 1050, 640);
            //form
            pb_Registration_Form.SetBounds(0, 1, 1050, 640);

            //name
            tbName.SetBounds(350, 136, 290, 42);
            //role
            cbRole.SetBounds(350, 202, 290, 42);
            //phoneNumber
            tbPhoneNumber.SetBounds(350, 262, 290, 42);
            //email
            tbEmail.SetBounds(350, 330, 290, 42);
            //Building Number
            cbBuilding.SetBounds(350, 400, 130, 42);
            cbRoom.SetBounds(500, 400, 130, 42);
            //button
            registerBtn.SetBounds(360, 438, 266, 42);
            backToLoginBtn.SetBounds(10, 10, 100, 42);
            Height = registrationPnl.Height;
            Width = registrationPnl.Width;
            MaximizeBox = false;
            CenterToScreen();

        }
        #endregion

        #region Login
        private void logInBtn_Click(object sender, EventArgs e)
        {
            try
            {
                string idPattern = "^$|^\\d{6}$"; //checking for right amount of digits
                Regex regex = new Regex(idPattern);
                int idValue = int.Parse(idTb.Text);
                if (regex.IsMatch(idTb.Text))
                {
                    switch (dBConnect.SelectPCheck(idValue))
                    {
                        case "S":
                            StudenPnl.Visible = true;
                            StudenPnl.SetBounds(0, 1, 1138, 640);
                            pbStudent.SetBounds(0, 1, 1138, 640);
                            btnStudent_Add.SetBounds(77, 470, 266, 42);
                            btnStudent_Home.SetBounds(85, 520, 250, 35);
                            rulesBtn.SetBounds(768, 520, 250, 35);
                            btnStudent_Change.SetBounds(756, 470, 266, 42);
                            lbStudent.SetBounds(60, 25, 1000, 410);
                            btnChatStudent.SetBounds(505, 520, 120, 42);
                            Height = StudenPnl.Height;
                            Width = StudenPnl.Width;
                            MaximizeBox = false;
                            CenterToScreen();
                            launchPnl.Visible = false;
                            MessageBox.Show("Hello, " + dBConnect.NameByID(idValue));
                            break;
                        case "M":
                            ManagerPnl.Visible = true;
                            ManagerPnl.SetBounds(0, 1, 1138, 640);
                            //picture
                            pb_Manager.SetBounds(0, 1, 1138, 640);
                            //button Add
                            btn_Manager_Add.SetBounds(77, 470, 266, 42);
                            //button Change
                            btn_Manager_Change.SetBounds(756, 470, 266, 42);
                            //listbox
                            lbManager.SetBounds(60, 25, 1000, 410);
                            btn_Manager_Home.SetBounds(115, 530, 196, 40);
                            Height = ManagerPnl.Height;
                            Width = ManagerPnl.Width;
                            MaximizeBox = false;
                            MinimizeBox = false;
                            CenterToScreen();
                            launchPnl.Visible = false;
                            MessageBox.Show("Hello, " + dBConnect.NameByID(idValue));
                            break;
                        case "NA":
                            MessageBox.Show(Properties.Resources.no_such_id);
                            break;
                        case "ERR":
                            MessageBox.Show(Properties.Resources.error_msg);
                            break;
                    }
                }
                else
                {
                    MessageBox.Show(Properties.Resources.exact_count_numbers);
                }
            }
            catch (FormatException)
            {
                MessageBox.Show(Properties.Resources.only_numbers);
            }
        }
        #endregion

        #region Registration
        private void backToLoginBtn_Click(object sender, EventArgs e)
        {
            launchPnl.Visible = true;
            registrationPnl.Visible = false;
        }
        private void registerBtn_Click(object sender, EventArgs e)
        {
            try
            {
                string name = tbName.Text;
                if (!string.IsNullOrWhiteSpace(name))
                {
                    string email = tbEmail.Text;
                    string emailPattern = "^[a-zA-Z0-9.!#$%&'*+/=?^_`{|}~-]+@[a-zA-Z0-9](?:[a-zA-Z0-9-]{0,61}[a-zA-Z0-9])?(?:\\.[a-zA-Z0-9](?:[a-zA-Z0-9-]{0,61}[a-zA-Z0-9])?)*$";
                    Regex regexE = new Regex(emailPattern);
                    if (regexE.IsMatch(email))
                    {
                        string phoneNumber = tbPhoneNumber.Text;
                        string phonePattern = "^[0-9]{10}$";
                        Regex regexP = new Regex(phonePattern);
                        if (regexP.IsMatch(phoneNumber))
                        {
                            string building = "";
                            string room = "";
                            if (cbBuilding.SelectedIndex > -1)
                            {
                                building = cbBuilding.SelectedItem.ToString();
                            }
                            if (cbRoom.SelectedIndex > -1)
                            {
                                room = cbRoom.SelectedItem.ToString();
                            }

                            if (cbRole.SelectedIndex > -1 && cbRole.SelectedIndex == 1)
                            {
                                Person person = new Person(name, phoneNumber, email, room, (TypeOfPerson)cbRole.SelectedIndex);
                                dBConnect.Insert($"INSERT INTO `managers` (`id`, `Name`, `Email`, `verification`, `PhoneNumber`, `identificationM`) VALUES (NULL, '{name}', '{email}', '{person.IdNumber}', '{phoneNumber}', 7777);");
                                MessageBox.Show(Properties.Resources.id_num_notification + person.IdNumber, Properties.Resources.id_num, MessageBoxButtons.OK, MessageBoxIcon.Information);
                            }
                            else if (!string.IsNullOrWhiteSpace(building) && !string.IsNullOrWhiteSpace(room) && cbRole.SelectedIndex > -1 && cbRole.SelectedIndex == 0)
                            {

                                if (dBConnect.RoomAvailable(room, building))
                                {
                                    Person person = new Person(name, phoneNumber, email, room, (TypeOfPerson)cbRole.SelectedIndex);
                                    dBConnect.Insert($"INSERT INTO `students` (`Id`, `Name`, `Email`, `Phone Number`, `Building`, `Room`, `identification`) VALUES (NULL, '{name}', '{email}', '{phoneNumber}', '{building}', '{room}', '{person.IdNumber}');");
                                    MessageBox.Show(Properties.Resources.id_num_notification + person.IdNumber, Properties.Resources.id_num, MessageBoxButtons.OK, MessageBoxIcon.Information);
                                }
                                else 
                                {
                                    MessageBox.Show(Properties.Resources.room_taken);
                                }
                            }
                            else
                            {
                                MessageBox.Show(Properties.Resources.no_role_building_room, Properties.Resources.error, MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
                            }
                        }
                        else
                        {
                            MessageBox.Show(Properties.Resources.invalid_phone_num, Properties.Resources.error, MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
                        }
                    }
                    else
                    {
                        MessageBox.Show(Properties.Resources.invalid_email_addr, Properties.Resources.error, MessageBoxButtons.OK, MessageBoxIcon.Exclamation); ;
                    }
                }
                else
                {
                    MessageBox.Show(Properties.Resources.invalid_name, Properties.Resources.error, MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
                }
            }
            catch (FormatException)
            {
                MessageBox.Show(Properties.Resources.wrong_format);
            }
            catch (ArgumentNullException)
            {
                MessageBox.Show(Properties.Resources.error_msg);
            }
        }
        private void cbRole_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (cbRole.SelectedIndex == 1)
            {
                cbRoom.Enabled = false;
                cbBuilding.Enabled = false;
            }
            else
            {
                cbRoom.Enabled = true;
                cbBuilding.Enabled = true;
            }
        }
        #endregion

        #region Student
        private void btnStudent_Add_Click(object sender, EventArgs e)
        {
            studentAdd student_Add = new studentAdd(dBConnect.NameByID(int.Parse(idTb.Text)));
            student_Add.Show();
        }
        private void btnStudent_Change_Click(object sender, EventArgs e)
        {

            if (lbStudent.SelectedIndex != -1)
            {
                int index = int.Parse(lbStudent.SelectedItem.ToString().Substring(0, 1));
                Student_Change student_Change = new Student_Change(index);
                student_Change.Show();
            }
            else
            {
                MessageBox.Show(Properties.Resources.select_event_msg);
            }

        }
        private void btnStudent_Home_Click(object sender, EventArgs e)
        {
            launchPnl.Visible = true;
            StudenPnl.Visible = false;
        }
        private void rulesBtn_Click(object sender, EventArgs e)
        {
            StudentRules form = new StudentRules();
            form.Show();
        }
        private void btnChatStudent_Click(object sender, EventArgs e)
        {
            ChatForm chatForm = new ChatForm(dBConnect.NameByID(int.Parse(idTb.Text)), dBConnect.BuildingByID(int.Parse(idTb.Text)));
            chatForm.Show();
        }
        private void timerStudent_Tick(object sender, EventArgs e)
        {
            lbStudent.Items.Clear();
            lbManager.Items.Clear();
            lbManager.Items.Add(Properties.Resources.agreements_label);
            foreach (var item in dBConnect.MandatoryEvents())
            {
                lbManager.Items.Add(item);
            }

            if (!string.IsNullOrWhiteSpace(idTb.Text))
            {
                foreach (var item in dBConnect.EventsByName(dBConnect.NameByID(int.Parse(idTb.Text))))
                {
                    lbStudent.Items.Add(item);
                }
            }
        }
        #endregion

        #region Manager        
        private void btn_Manager_Add_Click(object sender, EventArgs e)
        {
            Manager_Add form = new Manager_Add();
            form.Show();
        }
        private void btn_Manager_Change_Click(object sender, EventArgs e)
        {
            if (lbManager.SelectedIndex != -1)
            {
                try
                {
                    int index = int.Parse(lbManager.SelectedItem.ToString().Substring(0, 1));
                    Manager_Change manager_Change = new Manager_Change(index);
                    manager_Change.Show();
                }
                catch (FormatException)
                {
                    MessageBox.Show(Properties.Resources.valid_event_msg);
                }
            }
            else
            {
                MessageBox.Show(Properties.Resources.select_event_msg);
            }
        }
        private void btn_Manager_Home_Click(object sender, EventArgs e)
        {
            launchPnl.Visible = true;
            ManagerPnl.Visible = false;
        }
        private void complaintBtn_Click(object sender, EventArgs e)
        {
            ComplaintForm complaintForm = new ComplaintForm();
            complaintForm.Show();
        }
        #endregion
    }
}