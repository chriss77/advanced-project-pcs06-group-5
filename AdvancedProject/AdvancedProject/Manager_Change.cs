﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace AdvancedProject
{
    public partial class Manager_Change : Form
    {
        DBConnect dBConnect;
        string[] eventDetails;
        private int id;

        public Manager_Change(int id)
        {
            InitializeComponent();
            dBConnect = new DBConnect();
            this.id = id;
            dBConnect = new DBConnect();
            eventDetails = dBConnect.EventByIndex(id);
            tbName_ManagerForm.Text = eventDetails[1];
            tbDesc_ManagerForm.Text = eventDetails[2];
        }
       
        private void Manager_Change_Load(object sender, EventArgs e)
        {
            foreach (var item in dBConnect.AllStudents())
            {
                studentLb.Items.Add(item);
            }
            studentLb.SelectedItem = eventDetails[0];
        }

        private void editBtn_Click(object sender, EventArgs e)
        {
            dBConnect.Update($"UPDATE `events` SET `id`={id},`Person`='{studentLb.SelectedItem.ToString()}',`Title`='{tbName_ManagerForm.Text}',`Description`='{tbDesc_ManagerForm.Text}',`Date`='{dateTimePickerManager.Value.ToString("yyyy-MM-dd")}',`Type`='Mandatory' WHERE id = '{id}'");
        }

        private void deleteBtn_Click(object sender, EventArgs e)
        {
            dBConnect.Delete($"DELETE FROM `events` WHERE id = '{id}'");
        }
    }
}
