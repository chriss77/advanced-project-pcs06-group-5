﻿namespace AdvancedProject
{
    partial class studentAdd
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(studentAdd));
            this.btnAdd = new System.Windows.Forms.Button();
            this.typeLbl = new System.Windows.Forms.Label();
            this.typeOfEventCb = new System.Windows.Forms.ComboBox();
            this.dateTimePickerStudent = new System.Windows.Forms.DateTimePicker();
            this.tbName_StudentForm = new System.Windows.Forms.TextBox();
            this.label3 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.tbDesc_StudentForm = new System.Windows.Forms.TextBox();
            this.SuspendLayout();
            // 
            // btnAdd
            // 
            this.btnAdd.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("btnAdd.BackgroundImage")));
            this.btnAdd.Font = new System.Drawing.Font("Century Gothic", 11F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnAdd.ForeColor = System.Drawing.Color.Linen;
            this.btnAdd.Location = new System.Drawing.Point(313, 333);
            this.btnAdd.Name = "btnAdd";
            this.btnAdd.Size = new System.Drawing.Size(187, 82);
            this.btnAdd.TabIndex = 31;
            this.btnAdd.Text = "Add";
            this.btnAdd.UseVisualStyleBackColor = true;
            this.btnAdd.Click += new System.EventHandler(this.btnAdd_Click);
            // 
            // typeLbl
            // 
            this.typeLbl.AutoSize = true;
            this.typeLbl.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.typeLbl.Font = new System.Drawing.Font("Century Gothic", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.typeLbl.ForeColor = System.Drawing.Color.DarkGoldenrod;
            this.typeLbl.Location = new System.Drawing.Point(438, 116);
            this.typeLbl.Name = "typeLbl";
            this.typeLbl.Size = new System.Drawing.Size(62, 28);
            this.typeLbl.TabIndex = 30;
            this.typeLbl.Text = "TYPE";
            this.typeLbl.Click += new System.EventHandler(this.typeLbl_Click);
            // 
            // typeOfEventCb
            // 
            this.typeOfEventCb.FormattingEnabled = true;
            this.typeOfEventCb.Items.AddRange(new object[] {
            "Personal",
            "Mandatory",
            "Complaint"});
            this.typeOfEventCb.Location = new System.Drawing.Point(538, 116);
            this.typeOfEventCb.Name = "typeOfEventCb";
            this.typeOfEventCb.Size = new System.Drawing.Size(235, 28);
            this.typeOfEventCb.TabIndex = 29;
            this.typeOfEventCb.SelectedIndexChanged += new System.EventHandler(this.typeOfEventCb_SelectedIndexChanged);
            // 
            // dateTimePickerStudent
            // 
            this.dateTimePickerStudent.Location = new System.Drawing.Point(289, 36);
            this.dateTimePickerStudent.Name = "dateTimePickerStudent";
            this.dateTimePickerStudent.Size = new System.Drawing.Size(304, 26);
            this.dateTimePickerStudent.TabIndex = 28;
            this.dateTimePickerStudent.ValueChanged += new System.EventHandler(this.dateTimePickerStudent_ValueChanged);
            // 
            // tbName_StudentForm
            // 
            this.tbName_StudentForm.Location = new System.Drawing.Point(134, 118);
            this.tbName_StudentForm.Name = "tbName_StudentForm";
            this.tbName_StudentForm.Size = new System.Drawing.Size(235, 26);
            this.tbName_StudentForm.TabIndex = 27;
            this.tbName_StudentForm.TextChanged += new System.EventHandler(this.tbName_StudentForm_TextChanged);
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.label3.Font = new System.Drawing.Font("Century Gothic", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label3.ForeColor = System.Drawing.Color.DarkGoldenrod;
            this.label3.Location = new System.Drawing.Point(28, 118);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(82, 28);
            this.label3.TabIndex = 26;
            this.label3.Text = "NAME";
            this.label3.Click += new System.EventHandler(this.label3_Click);
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.label2.Font = new System.Drawing.Font("Century Gothic", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.ForeColor = System.Drawing.Color.DarkGoldenrod;
            this.label2.Location = new System.Drawing.Point(199, 36);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(69, 28);
            this.label2.TabIndex = 25;
            this.label2.Text = "DATE";
            this.label2.Click += new System.EventHandler(this.label2_Click);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.label1.Font = new System.Drawing.Font("Century Gothic", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.ForeColor = System.Drawing.Color.DarkGoldenrod;
            this.label1.Location = new System.Drawing.Point(28, 179);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(161, 28);
            this.label1.TabIndex = 24;
            this.label1.Text = "DESCRIPTION";
            this.label1.Click += new System.EventHandler(this.label1_Click);
            // 
            // tbDesc_StudentForm
            // 
            this.tbDesc_StudentForm.Font = new System.Drawing.Font("Century Gothic", 11F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.tbDesc_StudentForm.Location = new System.Drawing.Point(33, 220);
            this.tbDesc_StudentForm.Multiline = true;
            this.tbDesc_StudentForm.Name = "tbDesc_StudentForm";
            this.tbDesc_StudentForm.Size = new System.Drawing.Size(740, 91);
            this.tbDesc_StudentForm.TabIndex = 23;
            this.tbDesc_StudentForm.TextChanged += new System.EventHandler(this.tbDesc_StudentForm_TextChanged);
            // 
            // studentAdd
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(9F, 20F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("$this.BackgroundImage")));
            this.ClientSize = new System.Drawing.Size(800, 450);
            this.Controls.Add(this.btnAdd);
            this.Controls.Add(this.typeLbl);
            this.Controls.Add(this.typeOfEventCb);
            this.Controls.Add(this.dateTimePickerStudent);
            this.Controls.Add(this.tbName_StudentForm);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.tbDesc_StudentForm);
            this.Name = "studentAdd";
            this.Text = "Add Event";
            this.Load += new System.EventHandler(this.studentAdd_Load);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button btnAdd;
        private System.Windows.Forms.Label typeLbl;
        private System.Windows.Forms.ComboBox typeOfEventCb;
        private System.Windows.Forms.DateTimePicker dateTimePickerStudent;
        private System.Windows.Forms.TextBox tbName_StudentForm;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.TextBox tbDesc_StudentForm;
    }
}