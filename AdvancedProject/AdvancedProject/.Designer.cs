﻿namespace AdvancedProject
{
    partial class LoginRegistration
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(LoginRegistration));
            this.registrationPnl = new System.Windows.Forms.Panel();
            this.backToLoginBtn = new System.Windows.Forms.Button();
            this.tbEmail = new System.Windows.Forms.TextBox();
            this.cbRoom = new System.Windows.Forms.ComboBox();
            this.cbBuilding = new System.Windows.Forms.ComboBox();
            this.cbRole = new System.Windows.Forms.ComboBox();
            this.registerBtn = new System.Windows.Forms.Button();
            this.tbPhoneNumber = new System.Windows.Forms.TextBox();
            this.tbName = new System.Windows.Forms.TextBox();
            this.pb_Registration_Form = new System.Windows.Forms.PictureBox();
            this.launchPnl = new System.Windows.Forms.Panel();
            this.goToRegisterBtn = new System.Windows.Forms.Button();
            this.logInBtn = new System.Windows.Forms.Button();
            this.idTb = new System.Windows.Forms.TextBox();
            this.pb_LogIn_Form = new System.Windows.Forms.PictureBox();
            this.StudenPnl = new System.Windows.Forms.Panel();
            this.btnStudent_Home = new System.Windows.Forms.Button();
            this.rulesBtn = new System.Windows.Forms.Button();
            this.btnChatStudent = new System.Windows.Forms.Button();
            this.lbStudent = new System.Windows.Forms.ListBox();
            this.btnStudent_Add = new System.Windows.Forms.Button();
            this.btnStudent_Change = new System.Windows.Forms.Button();
            this.cbStudent_Schedule = new System.Windows.Forms.ComboBox();
            this.pbStudent = new System.Windows.Forms.PictureBox();
            this.ManagerPnl = new System.Windows.Forms.Panel();
            this.complaintsBtn = new System.Windows.Forms.Button();
            this.btn_Manager_Home = new System.Windows.Forms.Button();
            this.lbManager = new System.Windows.Forms.ListBox();
            this.btn_Manager_Add = new System.Windows.Forms.Button();
            this.btn_Manager_Change = new System.Windows.Forms.Button();
            this.pb_Manager = new System.Windows.Forms.PictureBox();
            this.timer1 = new System.Windows.Forms.Timer(this.components);
            this.timerStudent = new System.Windows.Forms.Timer(this.components);
            this.registrationPnl.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pb_Registration_Form)).BeginInit();
            this.launchPnl.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pb_LogIn_Form)).BeginInit();
            this.StudenPnl.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pbStudent)).BeginInit();
            this.ManagerPnl.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pb_Manager)).BeginInit();
            this.SuspendLayout();
            // 
            // registrationPnl
            // 
            this.registrationPnl.Controls.Add(this.backToLoginBtn);
            this.registrationPnl.Controls.Add(this.tbEmail);
            this.registrationPnl.Controls.Add(this.cbRoom);
            this.registrationPnl.Controls.Add(this.cbBuilding);
            this.registrationPnl.Controls.Add(this.cbRole);
            this.registrationPnl.Controls.Add(this.registerBtn);
            this.registrationPnl.Controls.Add(this.tbPhoneNumber);
            this.registrationPnl.Controls.Add(this.tbName);
            this.registrationPnl.Controls.Add(this.pb_Registration_Form);
            this.registrationPnl.Location = new System.Drawing.Point(14, 228);
            this.registrationPnl.Name = "registrationPnl";
            this.registrationPnl.Size = new System.Drawing.Size(394, 215);
            this.registrationPnl.TabIndex = 3;
            // 
            // backToLoginBtn
            // 
            this.backToLoginBtn.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("backToLoginBtn.BackgroundImage")));
            this.backToLoginBtn.Font = new System.Drawing.Font("Century Gothic", 11F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.backToLoginBtn.ForeColor = System.Drawing.Color.Linen;
            this.backToLoginBtn.Location = new System.Drawing.Point(228, 77);
            this.backToLoginBtn.Name = "backToLoginBtn";
            this.backToLoginBtn.Size = new System.Drawing.Size(100, 36);
            this.backToLoginBtn.TabIndex = 19;
            this.backToLoginBtn.Text = "HOME";
            this.backToLoginBtn.UseVisualStyleBackColor = true;
            this.backToLoginBtn.Click += new System.EventHandler(this.backToLoginBtn_Click);
            // 
            // tbEmail
            // 
            this.tbEmail.Font = new System.Drawing.Font("Century Gothic", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.tbEmail.Location = new System.Drawing.Point(228, 18);
            this.tbEmail.Name = "tbEmail";
            this.tbEmail.Size = new System.Drawing.Size(88, 37);
            this.tbEmail.TabIndex = 18;
            // 
            // cbRoom
            // 
            this.cbRoom.Font = new System.Drawing.Font("Century Gothic", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cbRoom.FormattingEnabled = true;
            this.cbRoom.Items.AddRange(new object[] {
            "Room 1",
            "Room 2",
            "Room 3",
            "Room 4",
            "Room 5"});
            this.cbRoom.Location = new System.Drawing.Point(122, 61);
            this.cbRoom.Name = "cbRoom";
            this.cbRoom.Size = new System.Drawing.Size(88, 38);
            this.cbRoom.TabIndex = 17;
            // 
            // cbBuilding
            // 
            this.cbBuilding.Font = new System.Drawing.Font("Century Gothic", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cbBuilding.FormattingEnabled = true;
            this.cbBuilding.Items.AddRange(new object[] {
            "Building 1",
            "Building 2",
            "Building 3"});
            this.cbBuilding.Location = new System.Drawing.Point(121, 17);
            this.cbBuilding.Name = "cbBuilding";
            this.cbBuilding.Size = new System.Drawing.Size(88, 38);
            this.cbBuilding.TabIndex = 16;
            // 
            // cbRole
            // 
            this.cbRole.Font = new System.Drawing.Font("Century Gothic", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cbRole.FormattingEnabled = true;
            this.cbRole.Items.AddRange(new object[] {
            "Student ",
            "Manager"});
            this.cbRole.Location = new System.Drawing.Point(16, 49);
            this.cbRole.Name = "cbRole";
            this.cbRole.Size = new System.Drawing.Size(88, 38);
            this.cbRole.TabIndex = 15;
            this.cbRole.SelectedIndexChanged += new System.EventHandler(this.cbRole_SelectedIndexChanged);
            // 
            // registerBtn
            // 
            this.registerBtn.Font = new System.Drawing.Font("Century Gothic", 11F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.registerBtn.ForeColor = System.Drawing.SystemColors.ControlLight;
            this.registerBtn.Image = ((System.Drawing.Image)(resources.GetObject("registerBtn.Image")));
            this.registerBtn.Location = new System.Drawing.Point(144, 115);
            this.registerBtn.Name = "registerBtn";
            this.registerBtn.Size = new System.Drawing.Size(121, 36);
            this.registerBtn.TabIndex = 12;
            this.registerBtn.Text = "Register";
            this.registerBtn.UseVisualStyleBackColor = true;
            this.registerBtn.Click += new System.EventHandler(this.registerBtn_Click);
            // 
            // tbPhoneNumber
            // 
            this.tbPhoneNumber.Font = new System.Drawing.Font("Century Gothic", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.tbPhoneNumber.Location = new System.Drawing.Point(16, 92);
            this.tbPhoneNumber.Name = "tbPhoneNumber";
            this.tbPhoneNumber.Size = new System.Drawing.Size(88, 37);
            this.tbPhoneNumber.TabIndex = 11;
            // 
            // tbName
            // 
            this.tbName.Font = new System.Drawing.Font("Century Gothic", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.tbName.Location = new System.Drawing.Point(16, 9);
            this.tbName.Name = "tbName";
            this.tbName.Size = new System.Drawing.Size(88, 37);
            this.tbName.TabIndex = 9;
            // 
            // pb_Registration_Form
            // 
            this.pb_Registration_Form.Image = ((System.Drawing.Image)(resources.GetObject("pb_Registration_Form.Image")));
            this.pb_Registration_Form.Location = new System.Drawing.Point(16, -2);
            this.pb_Registration_Form.Name = "pb_Registration_Form";
            this.pb_Registration_Form.Size = new System.Drawing.Size(325, 198);
            this.pb_Registration_Form.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.pb_Registration_Form.TabIndex = 13;
            this.pb_Registration_Form.TabStop = false;
            // 
            // launchPnl
            // 
            this.launchPnl.Controls.Add(this.goToRegisterBtn);
            this.launchPnl.Controls.Add(this.logInBtn);
            this.launchPnl.Controls.Add(this.idTb);
            this.launchPnl.Controls.Add(this.pb_LogIn_Form);
            this.launchPnl.Location = new System.Drawing.Point(14, 21);
            this.launchPnl.Name = "launchPnl";
            this.launchPnl.Size = new System.Drawing.Size(328, 184);
            this.launchPnl.TabIndex = 4;
            // 
            // goToRegisterBtn
            // 
            this.goToRegisterBtn.Font = new System.Drawing.Font("Century Gothic", 11F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.goToRegisterBtn.ForeColor = System.Drawing.SystemColors.ButtonHighlight;
            this.goToRegisterBtn.Image = ((System.Drawing.Image)(resources.GetObject("goToRegisterBtn.Image")));
            this.goToRegisterBtn.Location = new System.Drawing.Point(107, 128);
            this.goToRegisterBtn.Name = "goToRegisterBtn";
            this.goToRegisterBtn.Size = new System.Drawing.Size(122, 35);
            this.goToRegisterBtn.TabIndex = 5;
            this.goToRegisterBtn.Text = "Register";
            this.goToRegisterBtn.UseVisualStyleBackColor = true;
            this.goToRegisterBtn.Click += new System.EventHandler(this.goToRegisterBtn_Click);
            // 
            // logInBtn
            // 
            this.logInBtn.Font = new System.Drawing.Font("Century Gothic", 11F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.logInBtn.ForeColor = System.Drawing.Color.SeaShell;
            this.logInBtn.Image = ((System.Drawing.Image)(resources.GetObject("logInBtn.Image")));
            this.logInBtn.Location = new System.Drawing.Point(107, 86);
            this.logInBtn.Name = "logInBtn";
            this.logInBtn.Size = new System.Drawing.Size(100, 36);
            this.logInBtn.TabIndex = 4;
            this.logInBtn.Text = "LOG IN";
            this.logInBtn.UseVisualStyleBackColor = true;
            this.logInBtn.Click += new System.EventHandler(this.logInBtn_Click);
            // 
            // idTb
            // 
            this.idTb.Font = new System.Drawing.Font("Century Gothic", 14F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.idTb.Location = new System.Drawing.Point(81, 74);
            this.idTb.Multiline = true;
            this.idTb.Name = "idTb";
            this.idTb.Size = new System.Drawing.Size(148, 48);
            this.idTb.TabIndex = 3;
            // 
            // pb_LogIn_Form
            // 
            this.pb_LogIn_Form.BackColor = System.Drawing.SystemColors.AppWorkspace;
            this.pb_LogIn_Form.Image = ((System.Drawing.Image)(resources.GetObject("pb_LogIn_Form.Image")));
            this.pb_LogIn_Form.Location = new System.Drawing.Point(0, 3);
            this.pb_LogIn_Form.Name = "pb_LogIn_Form";
            this.pb_LogIn_Form.Size = new System.Drawing.Size(316, 184);
            this.pb_LogIn_Form.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.pb_LogIn_Form.TabIndex = 6;
            this.pb_LogIn_Form.TabStop = false;
            // 
            // StudenPnl
            // 
            this.StudenPnl.Controls.Add(this.btnStudent_Home);
            this.StudenPnl.Controls.Add(this.rulesBtn);
            this.StudenPnl.Controls.Add(this.btnChatStudent);
            this.StudenPnl.Controls.Add(this.lbStudent);
            this.StudenPnl.Controls.Add(this.btnStudent_Add);
            this.StudenPnl.Controls.Add(this.btnStudent_Change);
            this.StudenPnl.Controls.Add(this.cbStudent_Schedule);
            this.StudenPnl.Controls.Add(this.pbStudent);
            this.StudenPnl.Location = new System.Drawing.Point(382, 18);
            this.StudenPnl.Name = "StudenPnl";
            this.StudenPnl.Size = new System.Drawing.Size(302, 187);
            this.StudenPnl.TabIndex = 5;
            this.StudenPnl.Visible = false;
            // 
            // btnStudent_Home
            // 
            this.btnStudent_Home.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("btnStudent_Home.BackgroundImage")));
            this.btnStudent_Home.Font = new System.Drawing.Font("Century Gothic", 11F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnStudent_Home.ForeColor = System.Drawing.Color.Linen;
            this.btnStudent_Home.Location = new System.Drawing.Point(101, 75);
            this.btnStudent_Home.Name = "btnStudent_Home";
            this.btnStudent_Home.Size = new System.Drawing.Size(100, 36);
            this.btnStudent_Home.TabIndex = 21;
            this.btnStudent_Home.Text = "HOME";
            this.btnStudent_Home.UseVisualStyleBackColor = true;
            this.btnStudent_Home.Click += new System.EventHandler(this.btnStudent_Home_Click);
            // 
            // rulesBtn
            // 
            this.rulesBtn.Font = new System.Drawing.Font("Century Gothic", 11F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.rulesBtn.ForeColor = System.Drawing.SystemColors.ButtonHighlight;
            this.rulesBtn.Image = ((System.Drawing.Image)(resources.GetObject("rulesBtn.Image")));
            this.rulesBtn.Location = new System.Drawing.Point(6, 121);
            this.rulesBtn.Name = "rulesBtn";
            this.rulesBtn.Size = new System.Drawing.Size(121, 30);
            this.rulesBtn.TabIndex = 9;
            this.rulesBtn.Text = "Rules";
            this.rulesBtn.UseVisualStyleBackColor = true;
            this.rulesBtn.Click += new System.EventHandler(this.rulesBtn_Click);
            // 
            // btnChatStudent
            // 
            this.btnChatStudent.Font = new System.Drawing.Font("Century Gothic", 11F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnChatStudent.ForeColor = System.Drawing.Color.DarkGoldenrod;
            this.btnChatStudent.Image = ((System.Drawing.Image)(resources.GetObject("btnChatStudent.Image")));
            this.btnChatStudent.Location = new System.Drawing.Point(144, 121);
            this.btnChatStudent.Name = "btnChatStudent";
            this.btnChatStudent.Size = new System.Drawing.Size(89, 30);
            this.btnChatStudent.TabIndex = 8;
            this.btnChatStudent.Text = "CHAT";
            this.btnChatStudent.UseVisualStyleBackColor = true;
            this.btnChatStudent.Click += new System.EventHandler(this.btnChatStudent_Click);
            // 
            // lbStudent
            // 
            this.lbStudent.Font = new System.Drawing.Font("Century Gothic", 16F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbStudent.FormattingEnabled = true;
            this.lbStudent.ItemHeight = 39;
            this.lbStudent.Location = new System.Drawing.Point(6, 74);
            this.lbStudent.Name = "lbStudent";
            this.lbStudent.Size = new System.Drawing.Size(120, 4);
            this.lbStudent.TabIndex = 6;
            // 
            // btnStudent_Add
            // 
            this.btnStudent_Add.Font = new System.Drawing.Font("Century Gothic", 11F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnStudent_Add.ForeColor = System.Drawing.SystemColors.ButtonFace;
            this.btnStudent_Add.Image = ((System.Drawing.Image)(resources.GetObject("btnStudent_Add.Image")));
            this.btnStudent_Add.Location = new System.Drawing.Point(29, 35);
            this.btnStudent_Add.Name = "btnStudent_Add";
            this.btnStudent_Add.Size = new System.Drawing.Size(89, 30);
            this.btnStudent_Add.TabIndex = 3;
            this.btnStudent_Add.Text = "Add";
            this.btnStudent_Add.UseVisualStyleBackColor = true;
            this.btnStudent_Add.Click += new System.EventHandler(this.btnStudent_Add_Click);
            // 
            // btnStudent_Change
            // 
            this.btnStudent_Change.Font = new System.Drawing.Font("Century Gothic", 11F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnStudent_Change.ForeColor = System.Drawing.SystemColors.ButtonHighlight;
            this.btnStudent_Change.Image = ((System.Drawing.Image)(resources.GetObject("btnStudent_Change.Image")));
            this.btnStudent_Change.Location = new System.Drawing.Point(155, 35);
            this.btnStudent_Change.Name = "btnStudent_Change";
            this.btnStudent_Change.Size = new System.Drawing.Size(121, 30);
            this.btnStudent_Change.TabIndex = 2;
            this.btnStudent_Change.Text = "Change";
            this.btnStudent_Change.UseVisualStyleBackColor = true;
            this.btnStudent_Change.Click += new System.EventHandler(this.btnStudent_Change_Click);
            // 
            // cbStudent_Schedule
            // 
            this.cbStudent_Schedule.Font = new System.Drawing.Font("Century Gothic", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cbStudent_Schedule.FormattingEnabled = true;
            this.cbStudent_Schedule.Location = new System.Drawing.Point(132, 77);
            this.cbStudent_Schedule.Name = "cbStudent_Schedule";
            this.cbStudent_Schedule.Size = new System.Drawing.Size(121, 38);
            this.cbStudent_Schedule.TabIndex = 1;
            // 
            // pbStudent
            // 
            this.pbStudent.Image = ((System.Drawing.Image)(resources.GetObject("pbStudent.Image")));
            this.pbStudent.Location = new System.Drawing.Point(0, 6);
            this.pbStudent.Name = "pbStudent";
            this.pbStudent.Size = new System.Drawing.Size(244, 160);
            this.pbStudent.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.pbStudent.TabIndex = 7;
            this.pbStudent.TabStop = false;
            // 
            // ManagerPnl
            // 
            this.ManagerPnl.Controls.Add(this.complaintsBtn);
            this.ManagerPnl.Controls.Add(this.btn_Manager_Home);
            this.ManagerPnl.Controls.Add(this.lbManager);
            this.ManagerPnl.Controls.Add(this.btn_Manager_Add);
            this.ManagerPnl.Controls.Add(this.btn_Manager_Change);
            this.ManagerPnl.Controls.Add(this.pb_Manager);
            this.ManagerPnl.Location = new System.Drawing.Point(440, 237);
            this.ManagerPnl.Name = "ManagerPnl";
            this.ManagerPnl.Size = new System.Drawing.Size(284, 187);
            this.ManagerPnl.TabIndex = 6;
            this.ManagerPnl.Visible = false;
            // 
            // complaintsBtn
            // 
            this.complaintsBtn.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("complaintsBtn.BackgroundImage")));
            this.complaintsBtn.Font = new System.Drawing.Font("Century Gothic", 11F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.complaintsBtn.ForeColor = System.Drawing.Color.Linen;
            this.complaintsBtn.Location = new System.Drawing.Point(38, 123);
            this.complaintsBtn.Name = "complaintsBtn";
            this.complaintsBtn.Size = new System.Drawing.Size(100, 36);
            this.complaintsBtn.TabIndex = 21;
            this.complaintsBtn.Text = "COMPLAINTS";
            this.complaintsBtn.UseVisualStyleBackColor = true;
            this.complaintsBtn.Click += new System.EventHandler(this.complaintBtn_Click);
            // 
            // btn_Manager_Home
            // 
            this.btn_Manager_Home.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("btn_Manager_Home.BackgroundImage")));
            this.btn_Manager_Home.Font = new System.Drawing.Font("Century Gothic", 11F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btn_Manager_Home.ForeColor = System.Drawing.Color.Linen;
            this.btn_Manager_Home.Location = new System.Drawing.Point(144, 123);
            this.btn_Manager_Home.Name = "btn_Manager_Home";
            this.btn_Manager_Home.Size = new System.Drawing.Size(100, 36);
            this.btn_Manager_Home.TabIndex = 20;
            this.btn_Manager_Home.Text = "HOME";
            this.btn_Manager_Home.UseVisualStyleBackColor = true;
            this.btn_Manager_Home.Click += new System.EventHandler(this.btn_Manager_Home_Click);
            // 
            // lbManager
            // 
            this.lbManager.Font = new System.Drawing.Font("Century Gothic", 11F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbManager.FormattingEnabled = true;
            this.lbManager.ItemHeight = 25;
            this.lbManager.Location = new System.Drawing.Point(139, 77);
            this.lbManager.Name = "lbManager";
            this.lbManager.Size = new System.Drawing.Size(120, 29);
            this.lbManager.TabIndex = 7;
            // 
            // btn_Manager_Add
            // 
            this.btn_Manager_Add.Font = new System.Drawing.Font("Century Gothic", 11F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btn_Manager_Add.ForeColor = System.Drawing.SystemColors.ButtonHighlight;
            this.btn_Manager_Add.Image = ((System.Drawing.Image)(resources.GetObject("btn_Manager_Add.Image")));
            this.btn_Manager_Add.Location = new System.Drawing.Point(8, 35);
            this.btn_Manager_Add.Name = "btn_Manager_Add";
            this.btn_Manager_Add.Size = new System.Drawing.Size(89, 30);
            this.btn_Manager_Add.TabIndex = 3;
            this.btn_Manager_Add.Text = "Add";
            this.btn_Manager_Add.UseVisualStyleBackColor = true;
            this.btn_Manager_Add.Click += new System.EventHandler(this.btn_Manager_Add_Click);
            // 
            // btn_Manager_Change
            // 
            this.btn_Manager_Change.Font = new System.Drawing.Font("Century Gothic", 11F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btn_Manager_Change.ForeColor = System.Drawing.SystemColors.ButtonFace;
            this.btn_Manager_Change.Image = ((System.Drawing.Image)(resources.GetObject("btn_Manager_Change.Image")));
            this.btn_Manager_Change.Location = new System.Drawing.Point(103, 35);
            this.btn_Manager_Change.Name = "btn_Manager_Change";
            this.btn_Manager_Change.Size = new System.Drawing.Size(89, 30);
            this.btn_Manager_Change.TabIndex = 2;
            this.btn_Manager_Change.Text = "Change";
            this.btn_Manager_Change.UseVisualStyleBackColor = true;
            this.btn_Manager_Change.Click += new System.EventHandler(this.btn_Manager_Change_Click);
            // 
            // pb_Manager
            // 
            this.pb_Manager.Image = ((System.Drawing.Image)(resources.GetObject("pb_Manager.Image")));
            this.pb_Manager.Location = new System.Drawing.Point(3, 3);
            this.pb_Manager.Name = "pb_Manager";
            this.pb_Manager.Size = new System.Drawing.Size(268, 184);
            this.pb_Manager.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.pb_Manager.TabIndex = 0;
            this.pb_Manager.TabStop = false;
            // 
            // timerStudent
            // 
            this.timerStudent.Interval = 10000;
            this.timerStudent.Tick += new System.EventHandler(this.timerStudent_Tick);
            // 
            // LoginRegistration
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(9F, 20F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1192, 543);
            this.Controls.Add(this.ManagerPnl);
            this.Controls.Add(this.StudenPnl);
            this.Controls.Add(this.launchPnl);
            this.Controls.Add(this.registrationPnl);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedDialog;
            this.Name = "LoginRegistration";
            this.Text = "Form1";
            this.Load += new System.EventHandler(this.LoginRegistration_Load);
            this.registrationPnl.ResumeLayout(false);
            this.registrationPnl.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pb_Registration_Form)).EndInit();
            this.launchPnl.ResumeLayout(false);
            this.launchPnl.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pb_LogIn_Form)).EndInit();
            this.StudenPnl.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.pbStudent)).EndInit();
            this.ManagerPnl.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.pb_Manager)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion
        private System.Windows.Forms.Panel registrationPnl;
        private System.Windows.Forms.Panel launchPnl;
        private System.Windows.Forms.Panel StudenPnl;
        private System.Windows.Forms.Button registerBtn;
        private System.Windows.Forms.TextBox tbPhoneNumber;
        private System.Windows.Forms.TextBox tbName;
        private System.Windows.Forms.TextBox idTb;
        private System.Windows.Forms.Button logInBtn;
        private System.Windows.Forms.Button goToRegisterBtn;
        private System.Windows.Forms.PictureBox pb_LogIn_Form;
        private System.Windows.Forms.PictureBox pb_Registration_Form;
        private System.Windows.Forms.ComboBox cbRoom;
        private System.Windows.Forms.ComboBox cbBuilding;
        private System.Windows.Forms.ComboBox cbRole;
        private System.Windows.Forms.Button btnStudent_Add;
        private System.Windows.Forms.Button btnStudent_Change;
        private System.Windows.Forms.ComboBox cbStudent_Schedule;
        private System.Windows.Forms.ListBox lbStudent;
        private System.Windows.Forms.Panel ManagerPnl;
        private System.Windows.Forms.ListBox lbManager;
        private System.Windows.Forms.Button btn_Manager_Add;
        private System.Windows.Forms.Button btn_Manager_Change;
        private System.Windows.Forms.PictureBox pb_Manager;
        private System.Windows.Forms.PictureBox pbStudent;
        private System.Windows.Forms.TextBox tbEmail;
        private System.Windows.Forms.Button backToLoginBtn;
        private System.Windows.Forms.Timer timer1;
        private System.Windows.Forms.Button btnChatStudent;
        private System.Windows.Forms.Button rulesBtn;
        private System.Windows.Forms.Button btnStudent_Home;
        private System.Windows.Forms.Button btn_Manager_Home;
        private System.Windows.Forms.Timer timerStudent;
        private System.Windows.Forms.Button complaintsBtn;
    }
}

