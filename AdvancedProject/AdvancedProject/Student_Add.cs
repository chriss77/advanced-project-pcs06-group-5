﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace AdvancedProject
{
    public partial class studentAdd : Form
    {
        DBConnect dBConnect;
        string name;
        
        public studentAdd(string name)
        {
            InitializeComponent();
            this.name = name;
            dBConnect = new DBConnect();
        }

        private void btnAdd_StudentForm_Click(object sender, EventArgs e)
        {
          

        }

        private void studentAdd_Load(object sender, EventArgs e)
        {

        }

        private void btnAdd_Click(object sender, EventArgs e)
        {
            if (!string.IsNullOrWhiteSpace(tbName_StudentForm.Text) && !string.IsNullOrWhiteSpace(tbDesc_StudentForm.Text))
            {
                if (typeOfEventCb.SelectedIndex != -1) 
                {
                    if (typeOfEventCb.SelectedIndex == 2)
                    {
                        dBConnect.Insert($"INSERT INTO `complaints` (`id`, `Title`, `Description`, `Date`) VALUES (NULL, '{tbName_StudentForm.Text}', '{tbDesc_StudentForm.Text}', '{dateTimePickerStudent.Value.ToString("yyyy-MM-dd")}');");
                    }
                    else
                    {
                        dBConnect.Insert($"INSERT INTO `events` (`id`, `Person`, `Title`, `Description`, `Date`, `Type`) VALUES (NULL, '{name}', '{tbName_StudentForm.Text}', '{tbDesc_StudentForm.Text}', '{dateTimePickerStudent.Value.ToString("yyyy-MM-dd")}', '{typeOfEventCb.SelectedItem.ToString()}');");
                    }
                    Close();
                }
                else
                {
                    MessageBox.Show("No Type was selected");
                }                
            }
            else 
            {
                MessageBox.Show("All fields should have text!");
            }
        }

        private void typeLbl_Click(object sender, EventArgs e)
        {

        }

        private void typeOfEventCb_SelectedIndexChanged(object sender, EventArgs e)
        {

        }

        private void dateTimePickerStudent_ValueChanged(object sender, EventArgs e)
        {

        }

        private void tbName_StudentForm_TextChanged(object sender, EventArgs e)
        {

        }

        private void label3_Click(object sender, EventArgs e)
        {

        }

        private void label2_Click(object sender, EventArgs e)
        {

        }

        private void label1_Click(object sender, EventArgs e)
        {

        }

        private void tbDesc_StudentForm_TextChanged(object sender, EventArgs e)
        {

        }
    }
}
