﻿namespace AdvancedProject
{
    partial class Student_Change
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Student_Change));
            this.dateTimePickerStudentChange = new System.Windows.Forms.DateTimePicker();
            this.btnDelete_Student_Change = new System.Windows.Forms.Button();
            this.lblName = new System.Windows.Forms.Label();
            this.tbName_Student_Change = new System.Windows.Forms.TextBox();
            this.btnEdit_Student_Change = new System.Windows.Forms.Button();
            this.lblDate = new System.Windows.Forms.Label();
            this.lblDescription = new System.Windows.Forms.Label();
            this.tbDescription_Student_Change = new System.Windows.Forms.TextBox();
            this.SuspendLayout();
            // 
            // dateTimePickerStudentChange
            // 
            this.dateTimePickerStudentChange.Location = new System.Drawing.Point(487, 69);
            this.dateTimePickerStudentChange.Name = "dateTimePickerStudentChange";
            this.dateTimePickerStudentChange.Size = new System.Drawing.Size(285, 26);
            this.dateTimePickerStudentChange.TabIndex = 53;
            // 
            // btnDelete_Student_Change
            // 
            this.btnDelete_Student_Change.Location = new System.Drawing.Point(199, 147);
            this.btnDelete_Student_Change.Name = "btnDelete_Student_Change";
            this.btnDelete_Student_Change.Size = new System.Drawing.Size(134, 64);
            this.btnDelete_Student_Change.TabIndex = 52;
            this.btnDelete_Student_Change.Text = "DELETE";
            this.btnDelete_Student_Change.UseVisualStyleBackColor = true;
            this.btnDelete_Student_Change.Click += new System.EventHandler(this.btnDelete_Student_Change_Click);
            // 
            // lblName
            // 
            this.lblName.AutoSize = true;
            this.lblName.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.lblName.Font = new System.Drawing.Font("Century Gothic", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblName.ForeColor = System.Drawing.Color.DarkGoldenrod;
            this.lblName.Location = new System.Drawing.Point(27, 72);
            this.lblName.Name = "lblName";
            this.lblName.Size = new System.Drawing.Size(82, 28);
            this.lblName.TabIndex = 51;
            this.lblName.Text = "NAME";
            // 
            // tbName_Student_Change
            // 
            this.tbName_Student_Change.Font = new System.Drawing.Font("Century Gothic", 11F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.tbName_Student_Change.Location = new System.Drawing.Point(117, 66);
            this.tbName_Student_Change.Name = "tbName_Student_Change";
            this.tbName_Student_Change.Size = new System.Drawing.Size(216, 34);
            this.tbName_Student_Change.TabIndex = 50;
            // 
            // btnEdit_Student_Change
            // 
            this.btnEdit_Student_Change.Location = new System.Drawing.Point(32, 147);
            this.btnEdit_Student_Change.Name = "btnEdit_Student_Change";
            this.btnEdit_Student_Change.Size = new System.Drawing.Size(137, 64);
            this.btnEdit_Student_Change.TabIndex = 49;
            this.btnEdit_Student_Change.Text = "EDIT";
            this.btnEdit_Student_Change.UseVisualStyleBackColor = true;
            this.btnEdit_Student_Change.Click += new System.EventHandler(this.btnEdit_Student_Change_Click);
            // 
            // lblDate
            // 
            this.lblDate.AutoSize = true;
            this.lblDate.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.lblDate.Font = new System.Drawing.Font("Century Gothic", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblDate.ForeColor = System.Drawing.Color.DarkGoldenrod;
            this.lblDate.Location = new System.Drawing.Point(389, 72);
            this.lblDate.Name = "lblDate";
            this.lblDate.Size = new System.Drawing.Size(69, 28);
            this.lblDate.TabIndex = 48;
            this.lblDate.Text = "DATE";
            // 
            // lblDescription
            // 
            this.lblDescription.AutoSize = true;
            this.lblDescription.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.lblDescription.Font = new System.Drawing.Font("Century Gothic", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblDescription.ForeColor = System.Drawing.Color.DarkGoldenrod;
            this.lblDescription.Location = new System.Drawing.Point(363, 137);
            this.lblDescription.Name = "lblDescription";
            this.lblDescription.Size = new System.Drawing.Size(142, 28);
            this.lblDescription.TabIndex = 47;
            this.lblDescription.Text = "Description";
            // 
            // tbDescription_Student_Change
            // 
            this.tbDescription_Student_Change.Font = new System.Drawing.Font("Century Gothic", 11F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.tbDescription_Student_Change.Location = new System.Drawing.Point(511, 119);
            this.tbDescription_Student_Change.Multiline = true;
            this.tbDescription_Student_Change.Name = "tbDescription_Student_Change";
            this.tbDescription_Student_Change.Size = new System.Drawing.Size(216, 102);
            this.tbDescription_Student_Change.TabIndex = 46;
            // 
            // Student_Change
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(9F, 20F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("$this.BackgroundImage")));
            this.ClientSize = new System.Drawing.Size(800, 318);
            this.Controls.Add(this.dateTimePickerStudentChange);
            this.Controls.Add(this.btnDelete_Student_Change);
            this.Controls.Add(this.lblName);
            this.Controls.Add(this.tbName_Student_Change);
            this.Controls.Add(this.btnEdit_Student_Change);
            this.Controls.Add(this.lblDate);
            this.Controls.Add(this.lblDescription);
            this.Controls.Add(this.tbDescription_Student_Change);
            this.Name = "Student_Change";
            this.Text = "Student_Change";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.DateTimePicker dateTimePickerStudentChange;
        private System.Windows.Forms.Button btnDelete_Student_Change;
        private System.Windows.Forms.Label lblName;
        private System.Windows.Forms.TextBox tbName_Student_Change;
        private System.Windows.Forms.Button btnEdit_Student_Change;
        private System.Windows.Forms.Label lblDate;
        private System.Windows.Forms.Label lblDescription;
        private System.Windows.Forms.TextBox tbDescription_Student_Change;
    }
}