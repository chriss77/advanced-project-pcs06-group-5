﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace AdvancedProject
{
    public partial class ComplaintForm : Form
    {
        DBConnect dBConnect;
        public ComplaintForm()
        {
            InitializeComponent();
            dBConnect = new DBConnect();
        }

        private void resolvedBtn_Click(object sender, EventArgs e)
        {
            if (complaintsLb.SelectedIndex != -1)
            {
                try 
                { 
                    int index = int.Parse(complaintsLb.SelectedItem.ToString().Substring(0, 1));
                    dBConnect.Delete($"DELETE FROM `complaints` WHERE id = '{index}'");
                    MessageBox.Show(Properties.Resources.success);
                    Close();
                }
                catch (FormatException) 
                {
                    MessageBox.Show("Select a valid complaint first, please!"); 
                }
                
                
            }
            else
            {
                MessageBox.Show("Select a complaint first, please!");
            }
        }

        private void closeBtn_Click(object sender, EventArgs e)
        {
            Close();
        }

        private void ComplaintForm_Load(object sender, EventArgs e)
        {
            complaintsLb.Items.Add("Complaints:");
            foreach (var item in dBConnect.Complaints())
            {
                complaintsLb.Items.Add(item);
            }
        }
    }
}
