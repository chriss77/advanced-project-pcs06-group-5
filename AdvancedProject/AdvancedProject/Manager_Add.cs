﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace AdvancedProject
{

    public partial class Manager_Add : Form
    {
        DBConnect dBConnect;
        public Manager_Add()
        {
            InitializeComponent();
            dBConnect = new DBConnect();
        }
        private void btnAdd_Click(object sender, EventArgs e)
        {
            if (!string.IsNullOrWhiteSpace(tbName_ManagerForm.Text) && !string.IsNullOrWhiteSpace(tbDesc_ManagerForm.Text))
            {
                if (studentLb.SelectedIndex != -1)
                {
                    dBConnect.Insert($"INSERT INTO `events` (`id`, `Person`, `Title`, `Description`, `Date`, `Type`) VALUES (NULL, '{studentLb.SelectedItem.ToString()}', '{tbName_ManagerForm.Text}', '{tbDesc_ManagerForm.Text}', '{dateTimePickerManager.Value.ToString("yyyy-MM-dd")}', 'Mandatory');");
                    Close();
                }
                else 
                {
                    MessageBox.Show("Select a Student first");
                }
               
            }
            else
            {
                MessageBox.Show("All fields should have text!");
            }
        }

        private void Manager_Add_Load(object sender, EventArgs e)
        {
            foreach (var item in dBConnect.AllStudents())
            {
                studentLb.Items.Add(item);
            }
            
        }
    }
}
