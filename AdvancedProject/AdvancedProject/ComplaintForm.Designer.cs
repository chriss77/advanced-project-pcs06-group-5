﻿namespace AdvancedProject
{
    partial class ComplaintForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(ComplaintForm));
            this.complaintsLb = new System.Windows.Forms.ListBox();
            this.resolvedBtn = new System.Windows.Forms.Button();
            this.closeBtn = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // complaintsLb
            // 
            this.complaintsLb.FormattingEnabled = true;
            this.complaintsLb.ItemHeight = 20;
            this.complaintsLb.Location = new System.Drawing.Point(12, 12);
            this.complaintsLb.Name = "complaintsLb";
            this.complaintsLb.Size = new System.Drawing.Size(776, 364);
            this.complaintsLb.TabIndex = 0;
            // 
            // resolvedBtn
            // 
            this.resolvedBtn.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("resolvedBtn.BackgroundImage")));
            this.resolvedBtn.Font = new System.Drawing.Font("Century Gothic", 11F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.resolvedBtn.ForeColor = System.Drawing.Color.Linen;
            this.resolvedBtn.Location = new System.Drawing.Point(431, 393);
            this.resolvedBtn.Name = "resolvedBtn";
            this.resolvedBtn.Size = new System.Drawing.Size(238, 36);
            this.resolvedBtn.TabIndex = 23;
            this.resolvedBtn.Text = "Resolved";
            this.resolvedBtn.UseVisualStyleBackColor = true;
            this.resolvedBtn.Click += new System.EventHandler(this.resolvedBtn_Click);
            // 
            // closeBtn
            // 
            this.closeBtn.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("closeBtn.BackgroundImage")));
            this.closeBtn.Font = new System.Drawing.Font("Century Gothic", 11F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.closeBtn.ForeColor = System.Drawing.Color.Linen;
            this.closeBtn.Location = new System.Drawing.Point(134, 393);
            this.closeBtn.Name = "closeBtn";
            this.closeBtn.Size = new System.Drawing.Size(238, 36);
            this.closeBtn.TabIndex = 24;
            this.closeBtn.Text = "Close";
            this.closeBtn.UseVisualStyleBackColor = true;
            this.closeBtn.Click += new System.EventHandler(this.closeBtn_Click);
            // 
            // ComplaintForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(9F, 20F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(800, 450);
            this.Controls.Add(this.closeBtn);
            this.Controls.Add(this.resolvedBtn);
            this.Controls.Add(this.complaintsLb);
            this.Name = "ComplaintForm";
            this.Text = "ComplaintForm";
            this.Load += new System.EventHandler(this.ComplaintForm_Load);
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.ListBox complaintsLb;
        private System.Windows.Forms.Button resolvedBtn;
        private System.Windows.Forms.Button closeBtn;
    }
}