﻿namespace AdvancedProject
{
    partial class Manager_Change
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Manager_Change));
            this.studentLb = new System.Windows.Forms.ListBox();
            this.dateTimePickerManager = new System.Windows.Forms.DateTimePicker();
            this.tbName_ManagerForm = new System.Windows.Forms.TextBox();
            this.nameLbl = new System.Windows.Forms.Label();
            this.dateLbl = new System.Windows.Forms.Label();
            this.descLbl = new System.Windows.Forms.Label();
            this.tbDesc_ManagerForm = new System.Windows.Forms.TextBox();
            this.deleteBtn = new System.Windows.Forms.Button();
            this.editBtn = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // studentLb
            // 
            this.studentLb.FormattingEnabled = true;
            this.studentLb.ItemHeight = 20;
            this.studentLb.Location = new System.Drawing.Point(21, 24);
            this.studentLb.Name = "studentLb";
            this.studentLb.Size = new System.Drawing.Size(221, 384);
            this.studentLb.TabIndex = 49;
            // 
            // dateTimePickerManager
            // 
            this.dateTimePickerManager.Location = new System.Drawing.Point(349, 26);
            this.dateTimePickerManager.Name = "dateTimePickerManager";
            this.dateTimePickerManager.Size = new System.Drawing.Size(304, 26);
            this.dateTimePickerManager.TabIndex = 47;
            // 
            // tbName_ManagerForm
            // 
            this.tbName_ManagerForm.Location = new System.Drawing.Point(349, 68);
            this.tbName_ManagerForm.Name = "tbName_ManagerForm";
            this.tbName_ManagerForm.Size = new System.Drawing.Size(304, 26);
            this.tbName_ManagerForm.TabIndex = 46;
            // 
            // nameLbl
            // 
            this.nameLbl.AutoSize = true;
            this.nameLbl.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.nameLbl.Font = new System.Drawing.Font("Century Gothic", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.nameLbl.ForeColor = System.Drawing.Color.DarkGoldenrod;
            this.nameLbl.Location = new System.Drawing.Point(259, 64);
            this.nameLbl.Name = "nameLbl";
            this.nameLbl.Size = new System.Drawing.Size(82, 28);
            this.nameLbl.TabIndex = 45;
            this.nameLbl.Text = "NAME";
            // 
            // dateLbl
            // 
            this.dateLbl.AutoSize = true;
            this.dateLbl.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.dateLbl.Font = new System.Drawing.Font("Century Gothic", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.dateLbl.ForeColor = System.Drawing.Color.DarkGoldenrod;
            this.dateLbl.Location = new System.Drawing.Point(259, 24);
            this.dateLbl.Name = "dateLbl";
            this.dateLbl.Size = new System.Drawing.Size(69, 28);
            this.dateLbl.TabIndex = 44;
            this.dateLbl.Text = "DATE";
            // 
            // descLbl
            // 
            this.descLbl.AutoSize = true;
            this.descLbl.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.descLbl.Font = new System.Drawing.Font("Century Gothic", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.descLbl.ForeColor = System.Drawing.Color.DarkGoldenrod;
            this.descLbl.Location = new System.Drawing.Point(259, 119);
            this.descLbl.Name = "descLbl";
            this.descLbl.Size = new System.Drawing.Size(161, 28);
            this.descLbl.TabIndex = 43;
            this.descLbl.Text = "DESCRIPTION";
            // 
            // tbDesc_ManagerForm
            // 
            this.tbDesc_ManagerForm.Font = new System.Drawing.Font("Century Gothic", 11F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.tbDesc_ManagerForm.Location = new System.Drawing.Point(264, 165);
            this.tbDesc_ManagerForm.Multiline = true;
            this.tbDesc_ManagerForm.Name = "tbDesc_ManagerForm";
            this.tbDesc_ManagerForm.Size = new System.Drawing.Size(516, 159);
            this.tbDesc_ManagerForm.TabIndex = 42;
            // 
            // deleteBtn
            // 
            this.deleteBtn.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("deleteBtn.BackgroundImage")));
            this.deleteBtn.Font = new System.Drawing.Font("Century Gothic", 11F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.deleteBtn.ForeColor = System.Drawing.Color.Linen;
            this.deleteBtn.Location = new System.Drawing.Point(587, 351);
            this.deleteBtn.Name = "deleteBtn";
            this.deleteBtn.Size = new System.Drawing.Size(193, 57);
            this.deleteBtn.TabIndex = 50;
            this.deleteBtn.Text = "Delete";
            this.deleteBtn.UseVisualStyleBackColor = true;
            this.deleteBtn.Click += new System.EventHandler(this.deleteBtn_Click);
            // 
            // editBtn
            // 
            this.editBtn.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("editBtn.BackgroundImage")));
            this.editBtn.Font = new System.Drawing.Font("Century Gothic", 11F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.editBtn.ForeColor = System.Drawing.Color.Linen;
            this.editBtn.Location = new System.Drawing.Point(361, 351);
            this.editBtn.Name = "editBtn";
            this.editBtn.Size = new System.Drawing.Size(193, 57);
            this.editBtn.TabIndex = 51;
            this.editBtn.Text = "Edit";
            this.editBtn.UseVisualStyleBackColor = true;
            this.editBtn.Click += new System.EventHandler(this.editBtn_Click);
            // 
            // Manager_Change
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(9F, 20F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("$this.BackgroundImage")));
            this.ClientSize = new System.Drawing.Size(800, 437);
            this.Controls.Add(this.editBtn);
            this.Controls.Add(this.deleteBtn);
            this.Controls.Add(this.studentLb);
            this.Controls.Add(this.dateTimePickerManager);
            this.Controls.Add(this.tbName_ManagerForm);
            this.Controls.Add(this.nameLbl);
            this.Controls.Add(this.dateLbl);
            this.Controls.Add(this.descLbl);
            this.Controls.Add(this.tbDesc_ManagerForm);
            this.Name = "Manager_Change";
            this.Text = "Manager_Change";
            this.Load += new System.EventHandler(this.Manager_Change_Load);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.ListBox studentLb;
        private System.Windows.Forms.DateTimePicker dateTimePickerManager;
        private System.Windows.Forms.TextBox tbName_ManagerForm;
        private System.Windows.Forms.Label nameLbl;
        private System.Windows.Forms.Label dateLbl;
        private System.Windows.Forms.Label descLbl;
        private System.Windows.Forms.TextBox tbDesc_ManagerForm;
        private System.Windows.Forms.Button deleteBtn;
        private System.Windows.Forms.Button editBtn;
    }
}