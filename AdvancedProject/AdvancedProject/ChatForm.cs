﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace AdvancedProject
{
    public partial class ChatForm : Form
    {
        DBConnect dBConnect;
        string name;
        string building;

        public ChatForm(string person, string building)
        {
            InitializeComponent();
            dBConnect = new DBConnect();
            name = person;
            this.building = building;
            chatTimer.Start();
        }

        private void closeBtn_Click(object sender, EventArgs e)
        {
            Close();
        }

        private void sendBtn_Click(object sender, EventArgs e)
        {
            dBConnect.Insert($"INSERT INTO `chat` (`id`, `Person`, `Building`, `Message`) VALUES (NULL, '{name}', '{building}', '{messageRTb.Text}');");
        }

        private void chatTimer_Tick(object sender, EventArgs e)
        {
            chatLb.Items.Clear();
            foreach (var item in dBConnect.MessagesByBuilding(building))
            {
                chatLb.Items.Add(item);
            }
            
        }
    }
}
